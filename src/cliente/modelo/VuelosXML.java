/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Vuelos")
public class VuelosXML implements Serializable{
    private List<VueloXML> Vuelo;

    /**
     * @return the Vuelo
     */
    public List<VueloXML> getVuelo() {
        return Vuelo;
    }

    /**
     * @param Vuelo the Vuelo to set
     */
    public void setVuelo(List<VueloXML> Vuelo) {
        this.Vuelo = Vuelo;
    }
    
}
