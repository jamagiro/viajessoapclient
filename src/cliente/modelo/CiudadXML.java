/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Ciudad")
@XmlType(propOrder = {"codeCiudad", "descripcion","codePais"})
public class CiudadXML implements Serializable {
    private int codeCiudad;
    private String descripcion;
    private int  codePais;

    /**
     * @return the codeCiudad
     */
    public int getCodeCiudad() {
        return codeCiudad;
    }

    /**
     * @param codeCiudad the codeCiudad to set
     */
    public void setCodeCiudad(int codeCiudad) {
        this.codeCiudad = codeCiudad;
    }

    /**
     * @return the Descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param Descripcion the Descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codePais
     */
    public int getCodePais() {
        return codePais;
    }

    /**
     * @param codePais the codePais to set
     */
    public void setCodePais(int codePais) {
        this.codePais = codePais;
    }
    
}
