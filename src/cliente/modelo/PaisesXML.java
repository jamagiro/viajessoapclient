/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Paises")
public class PaisesXML implements Serializable {
    private List<PaisXML> Pais;

    /**
     * @return the Pais
     */
    public List<PaisXML> getPais() {
        return Pais;
    }

    /**
     * @param Pais the Pais to set
     */
    public void setPais(List<PaisXML> Pais) {
        this.Pais = Pais;
    }


}
