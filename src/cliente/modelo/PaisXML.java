/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Pais")
@XmlType(propOrder = {"codePais", "descripcion"})
public class PaisXML implements Serializable {
    private int codePais;
    private String descripcion;

    /**
     * @return the codePais
     */
    public int getCodePais() {
        return codePais;
    }

    /**
     * @param codePais the codePais to set
     */
    public void setCodePais(int codePais) {
        this.codePais = codePais;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
