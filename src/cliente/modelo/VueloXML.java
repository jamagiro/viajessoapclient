/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Vuelo")
//@XmlType(propOrder = {"codeVuelo", "origen","Destino", "fechaIda","horaSalida", "fechaVuelta","horaLlegada"})
public class VueloXML implements Serializable{

    private int codeVuelo;
    private String origen;
    private String Destino;
    private String fechaIda;
    private String horaSalida;
    private String fechaVuelta;
    private String horaLlegada;

    /**
     * @return the codeVuelo
     */
    public int getCodeVuelo() {
        return codeVuelo;
    }

    /**
     * @param codeVuelo the codeVuelo to set
     */
    @XmlElement (name ="codeVuelo" )
    public void setCodeVuelo(int codeVuelo) {
        this.codeVuelo = codeVuelo;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    @XmlElement (name ="origen" )
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the Destino
     */
    public String getDestino() {
        return Destino;
    }

    /**
     * @param Destino the Destino to set
     */
    @XmlElement (name ="Destino" )
    public void setDestino(String Destino) {
        this.Destino = Destino;
    }

    /**
     * @return the fechaIda
     */
    
    public String getFechaIda() {
        return fechaIda;
    }

    /**
     * @param fechaIda the fechaIda to set
     */
    @XmlElement (name ="fechaIda" )
    public void setFechaIda(String fechaIda) {
        this.fechaIda = fechaIda;
    }

    /**
     * @return the horaSalida
     */
    public String getHoraSalida() {
        return horaSalida;
    }

    /**
     * @param horaSalida the horaSalida to set
     */
    @XmlElement (name ="horaSalida" )
    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    /**
     * @return the fechaVuelta
     */
    public String getFechaVuelta() {
        return fechaVuelta;
    }

    /**
     * @param fechaVuelta the fechaVuelta to set
     */
    @XmlElement (name ="fechaVuelta" )
    public void setFechaVuelta(String fechaVuelta) {
        this.fechaVuelta = fechaVuelta;
    }

    /**
     * @return the horaLlegada
     */
    public String getHoraLlegada() {
        return horaLlegada;
    }

    /**
     * @param horaLlegada the horaLlegada to set
     */
    @XmlElement (name ="horaLlegada" )
    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

}
