/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente.modelo;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author STEJOCES
 */
@XmlRootElement (name="Ciudades")
public class CiudadesXML implements Serializable {
    private List<CiudadXML> Ciudad;    

    /**
     * @return the Ciudad
     */
    public List<CiudadXML> getCiudad() {
        return Ciudad;
    }

    /**
     * @param Ciudad the Ciudad to set
     */
    public void setCiudad(List<CiudadXML> Ciudad) {
        this.Ciudad = Ciudad;
    }
    
}
