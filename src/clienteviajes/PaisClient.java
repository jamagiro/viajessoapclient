
import cliente.modelo.PaisXML;
import cliente.modelo.PaisesXML;
import com.viajes.ws.pais.Pais;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jani
 */
public class PaisClient {

//    public static void main(String[] args) {
//        System.out.println("listaPaises:" + listPaises_1().size());
//        System.out.println("searchPais:" + searchPais(1));
//        System.out.println("activo:" + activo());
//
//        System.out.println("AquiXML Pais");
//        System.out.println(generaPaisXML(3));
//        System.out.println("AquiXML Paises");
//        System.out.println(generaPaisesXML());
//
//    }

    public List<Pais> listPaisesClient() {
        return listPaises_1();
    }

    public Pais searchPaisClient(Integer codPais) {
        return searchPais(codPais);
    }

    public String activoClient() {
        return activo();
    }

    private static String activo() {
        com.viajes.ws.pais.WsPais_Service service = new com.viajes.ws.pais.WsPais_Service();
        com.viajes.ws.pais.WsPais port = service.getWsPaisPort();
        return port.activo();
    }

    private static java.util.List<com.viajes.ws.pais.Pais> listPaises_1() {
        com.viajes.ws.pais.WsPais_Service service = new com.viajes.ws.pais.WsPais_Service();
        com.viajes.ws.pais.WsPais port = service.getWsPaisPort();
        return port.listPaises();
    }

    private static Pais searchPais(java.lang.Integer codPais) {
        com.viajes.ws.pais.WsPais_Service service = new com.viajes.ws.pais.WsPais_Service();
        com.viajes.ws.pais.WsPais port = service.getWsPaisPort();
        return port.searchPais(codPais);
    }

    public static String generaPaisXML(Integer code) {
        String xml;
        //System.out.println("Hotel XML-método");
        StringWriter w = new StringWriter();
        Pais pais = new Pais();
        pais = searchPais(code);
        try {
            PaisXML pXML = new PaisXML();
            pXML.setCodePais(pais.getCodPais());
            pXML.setDescripcion(pais.getDescripcion());

            JAXBContext context = JAXBContext.newInstance(PaisXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(pXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }

    public static String generaPaisesXML() {
        String xml;
        //System.out.println("Hoteles XML-método");
        StringWriter w = new StringWriter();
        List<Pais> lstPais = new ArrayList<Pais>();
        lstPais = listPaises_1();
        List<PaisXML> lstpXML = new ArrayList<PaisXML>();
        Integer size = lstPais.size();
        Integer n = 0;
        while (n < size) {
            PaisXML pXML = new PaisXML();
            pXML.setCodePais(lstPais.get(n).getCodPais());
            pXML.setDescripcion(lstPais.get(n).getDescripcion());
            lstpXML.add(pXML);
            n += 1;
        }

        try {

            PaisesXML psXML = new PaisesXML();
            psXML.setPais(lstpXML);

            JAXBContext context = JAXBContext.newInstance(PaisesXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(psXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }

}
