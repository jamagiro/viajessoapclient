//import cliente.modelo.CiudadXML;
//import cliente.modelo.CiudadesXML;

import cliente.modelo.VueloXML;
import cliente.modelo.VuelosXML;
import com.viajes.ws.ciudad.Ciudad;
import com.viajes.ws.vuelo.Vuelo;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jani
 */
public class VueloClient {

//    public static void main(String[] args) {
//        System.out.println("listaVuelos:" + listVuelos().size());
//        System.out.println("searchVuelo:" + searchVuelo(1));
//        System.out.println("activo:" + activo());
//        
//        System.out.println("AquiXML Vuelo");
//        System.out.println(generaVueloXML(1));
//        System.out.println("AquiXML Vuelos");
//        System.out.println(generaVuelosXML());        
//
//    }

    public List<Vuelo> listVuelosClient() {
        return listVuelos();
    }

    public Vuelo searchVueloClient(Integer codVuelo) {
        return searchVuelo(codVuelo);
    }

    public String activoClient() {
        return activo();
    }

    private static String activo() {
        com.viajes.ws.vuelo.WsVuelo_Service service = new com.viajes.ws.vuelo.WsVuelo_Service();
        com.viajes.ws.vuelo.WsVuelo port = service.getWsVueloPort();
        return port.activo();
    }

    private static java.util.List<com.viajes.ws.vuelo.Vuelo> listVuelos() {
        com.viajes.ws.vuelo.WsVuelo_Service service = new com.viajes.ws.vuelo.WsVuelo_Service();
        com.viajes.ws.vuelo.WsVuelo port = service.getWsVueloPort();
        return port.listVuelos();
    }

    private static Vuelo searchVuelo(java.lang.Integer codVuelo) {
        com.viajes.ws.vuelo.WsVuelo_Service service = new com.viajes.ws.vuelo.WsVuelo_Service();
        com.viajes.ws.vuelo.WsVuelo port = service.getWsVueloPort();
        return port.searchVuelo(codVuelo);
    }

    public static String generaVueloXML(Integer code) {
        String xml;
        //System.out.println("Hotel XML-método");
        StringWriter w = new StringWriter();
        Vuelo vuelo = new Vuelo();
        vuelo = searchVuelo(code);
        try {
            VueloXML vXML = new VueloXML();
            vXML.setCodeVuelo(vuelo.getCodVuelo());
            vXML.setOrigen(vuelo.getOrigen());
            vXML.setDestino(vuelo.getDestino());
            vXML.setFechaIda(vuelo.getFecIda());
            vXML.setHoraSalida(vuelo.getHoraSalida());
            vXML.setFechaVuelta(vuelo.getFecVuelta());
            vXML.setHoraLlegada(vuelo.getHoraLlegada());

            JAXBContext context = JAXBContext.newInstance(VueloXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(vXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }

    public static String generaVuelosXML() {
        String xml;
        //System.out.println("Hoteles XML-método");
        StringWriter w = new StringWriter();
        List<Vuelo> lstVuelo = new ArrayList<Vuelo>();
        lstVuelo = listVuelos();
        List<VueloXML> lstvXML = new ArrayList<VueloXML>();
        Integer size = lstVuelo.size();
        Integer n = 0;
        while (n < size) {
            VueloXML vXML = new VueloXML();
            vXML.setCodeVuelo(lstVuelo.get(n).getCodVuelo());
            vXML.setOrigen(lstVuelo.get(n).getOrigen());
            vXML.setDestino(lstVuelo.get(n).getDestino());
            vXML.setFechaIda(lstVuelo.get(n).getFecIda());
            vXML.setHoraSalida(lstVuelo.get(n).getHoraSalida());
            vXML.setFechaVuelta(lstVuelo.get(n).getFecVuelta());
            vXML.setHoraLlegada(lstVuelo.get(n).getHoraLlegada());

            lstvXML.add(vXML);
            n += 1;
        }

        try {

            VuelosXML vsXML = new VuelosXML();
            vsXML.setVuelo(lstvXML);

            JAXBContext context = JAXBContext.newInstance(VuelosXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(vsXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }

}
