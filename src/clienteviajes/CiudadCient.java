
import cliente.modelo.CiudadXML;
import cliente.modelo.CiudadesXML;
import com.viajes.ws.ciudad.Ciudad;
import com.viajes.ws.pais.Pais;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jani
 */
public class CiudadCient {

//    public static void main(String[] args) {
//        System.out.println("listaCiudades:" + listCiudades().size());
//        System.out.println("searchCiudad:" + searchCiudad(1));
//        System.out.println("activo:" + activo());
//        System.out.println("AquiXML Ciudad");
//        System.out.println(generaCiudadXML(3));
//        System.out.println("AquiXML Ciudades");
//        System.out.println(generaCiudadesXML());        
//    }

    public List<Ciudad> listCiudadesClient() {
        return listCiudades();
    }

    public Ciudad searchCiudadClient(Integer codCiudad) {
        return searchCiudad(codCiudad);
    }

    public String activoClient() {
        return activo();
    }

    private static String activo() {
        com.viajes.ws.ciudad.WsCiudad_Service service = new com.viajes.ws.ciudad.WsCiudad_Service();
        com.viajes.ws.ciudad.WsCiudad port = service.getWsCiudadPort();
        return port.activo();
    }

    private static java.util.List<com.viajes.ws.ciudad.Ciudad> listCiudades() {
        com.viajes.ws.ciudad.WsCiudad_Service service = new com.viajes.ws.ciudad.WsCiudad_Service();
        com.viajes.ws.ciudad.WsCiudad port = service.getWsCiudadPort();
        return port.listCiudades();
    }

    private static Ciudad searchCiudad(java.lang.Integer codCiudad) {
        com.viajes.ws.ciudad.WsCiudad_Service service = new com.viajes.ws.ciudad.WsCiudad_Service();
        com.viajes.ws.ciudad.WsCiudad port = service.getWsCiudadPort();
        return port.searchCiudad(codCiudad);
    }

    public static String generaCiudadXML(Integer code) {
        String xml;
        //System.out.println("Hotel XML-método");
        StringWriter w = new StringWriter();
        Ciudad ciudad = new Ciudad();
        ciudad = searchCiudad(code);
        try {
            CiudadXML cXML = new CiudadXML();
            cXML.setCodeCiudad(ciudad.getCodCiudad());
            cXML.setDescripcion(ciudad.getDescripcion());
            cXML.setCodePais(ciudad.getCodPais());

            JAXBContext context = JAXBContext.newInstance(CiudadXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(cXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }

    public static String generaCiudadesXML() {
        String xml;
        //System.out.println("Hoteles XML-método");
        StringWriter w = new StringWriter();
        List<Ciudad> lstCiudad = new ArrayList<Ciudad>();
        lstCiudad = listCiudades();
        List<CiudadXML> lstcXML = new ArrayList<CiudadXML>();
        Integer size = lstCiudad.size();
        Integer n = 0;
        while (n < size) {
            CiudadXML cXML = new CiudadXML();
            cXML.setCodeCiudad(lstCiudad.get(n).getCodCiudad());
            cXML.setDescripcion(lstCiudad.get(n).getDescripcion());
            cXML.setCodePais(lstCiudad.get(n).getCodPais());
            lstcXML.add(cXML);
            n += 1;
        }

        try {

            CiudadesXML csXML = new CiudadesXML();
            csXML.setCiudad(lstcXML);

            JAXBContext context = JAXBContext.newInstance(CiudadesXML.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(csXML, w);
        } catch (JAXBException ex) {
            System.out.println(ex.getMessage());
            //throw new BramantiException(ex.getMessage(), ex);
        }
        //System.out.println(w.toString());             
        xml = w.toString();
        return xml;
    }
    
}
