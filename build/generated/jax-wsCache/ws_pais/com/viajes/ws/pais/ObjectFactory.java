
package com.viajes.ws.pais;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.viajes.ws.pais package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListPaises_QNAME = new QName("http://ws.viajes/", "listPaises");
    private final static QName _SearchPaisResponse_QNAME = new QName("http://ws.viajes/", "searchPaisResponse");
    private final static QName _SearchPais_QNAME = new QName("http://ws.viajes/", "searchPais");
    private final static QName _ListPaisesResponse_QNAME = new QName("http://ws.viajes/", "listPaisesResponse");
    private final static QName _ActivoResponse_QNAME = new QName("http://ws.viajes/", "activoResponse");
    private final static QName _Activo_QNAME = new QName("http://ws.viajes/", "activo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.viajes.ws.pais
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListPaises }
     * 
     */
    public ListPaises createListPaises() {
        return new ListPaises();
    }

    /**
     * Create an instance of {@link SearchPaisResponse }
     * 
     */
    public SearchPaisResponse createSearchPaisResponse() {
        return new SearchPaisResponse();
    }

    /**
     * Create an instance of {@link SearchPais }
     * 
     */
    public SearchPais createSearchPais() {
        return new SearchPais();
    }

    /**
     * Create an instance of {@link ListPaisesResponse }
     * 
     */
    public ListPaisesResponse createListPaisesResponse() {
        return new ListPaisesResponse();
    }

    /**
     * Create an instance of {@link ActivoResponse }
     * 
     */
    public ActivoResponse createActivoResponse() {
        return new ActivoResponse();
    }

    /**
     * Create an instance of {@link Activo }
     * 
     */
    public Activo createActivo() {
        return new Activo();
    }

    /**
     * Create an instance of {@link Pais }
     * 
     */
    public Pais createPais() {
        return new Pais();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPaises }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listPaises")
    public JAXBElement<ListPaises> createListPaises(ListPaises value) {
        return new JAXBElement<ListPaises>(_ListPaises_QNAME, ListPaises.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPaisResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchPaisResponse")
    public JAXBElement<SearchPaisResponse> createSearchPaisResponse(SearchPaisResponse value) {
        return new JAXBElement<SearchPaisResponse>(_SearchPaisResponse_QNAME, SearchPaisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPais }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchPais")
    public JAXBElement<SearchPais> createSearchPais(SearchPais value) {
        return new JAXBElement<SearchPais>(_SearchPais_QNAME, SearchPais.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPaisesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listPaisesResponse")
    public JAXBElement<ListPaisesResponse> createListPaisesResponse(ListPaisesResponse value) {
        return new JAXBElement<ListPaisesResponse>(_ListPaisesResponse_QNAME, ListPaisesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activoResponse")
    public JAXBElement<ActivoResponse> createActivoResponse(ActivoResponse value) {
        return new JAXBElement<ActivoResponse>(_ActivoResponse_QNAME, ActivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activo")
    public JAXBElement<Activo> createActivo(Activo value) {
        return new JAXBElement<Activo>(_Activo_QNAME, Activo.class, null, value);
    }

}
