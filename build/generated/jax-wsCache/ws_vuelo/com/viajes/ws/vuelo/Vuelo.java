
package com.viajes.ws.vuelo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for vuelo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vuelo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codVuelo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecIda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecVuelta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaLlegada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vuelo", propOrder = {
    "codVuelo",
    "destino",
    "fecIda",
    "fecVuelta",
    "horaLlegada",
    "horaSalida",
    "origen"
})
public class Vuelo {

    protected int codVuelo;
    protected String destino;
    protected String fecIda;
    protected String fecVuelta;
    protected String horaLlegada;
    protected String horaSalida;
    protected String origen;

    /**
     * Gets the value of the codVuelo property.
     * 
     */
    public int getCodVuelo() {
        return codVuelo;
    }

    /**
     * Sets the value of the codVuelo property.
     * 
     */
    public void setCodVuelo(int value) {
        this.codVuelo = value;
    }

    /**
     * Gets the value of the destino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestino() {
        return destino;
    }

    /**
     * Sets the value of the destino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestino(String value) {
        this.destino = value;
    }

    /**
     * Gets the value of the fecIda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecIda() {
        return fecIda;
    }

    /**
     * Sets the value of the fecIda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecIda(String value) {
        this.fecIda = value;
    }

    /**
     * Gets the value of the fecVuelta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecVuelta() {
        return fecVuelta;
    }

    /**
     * Sets the value of the fecVuelta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecVuelta(String value) {
        this.fecVuelta = value;
    }

    /**
     * Gets the value of the horaLlegada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraLlegada() {
        return horaLlegada;
    }

    /**
     * Sets the value of the horaLlegada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraLlegada(String value) {
        this.horaLlegada = value;
    }

    /**
     * Gets the value of the horaSalida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraSalida() {
        return horaSalida;
    }

    /**
     * Sets the value of the horaSalida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraSalida(String value) {
        this.horaSalida = value;
    }

    /**
     * Gets the value of the origen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Sets the value of the origen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

}
