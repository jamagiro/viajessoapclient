
package com.viajes.ws.ciudad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ciudad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ciudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCiudad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codPais" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ciudad", propOrder = {
    "codCiudad",
    "codPais",
    "descripcion"
})
public class Ciudad {

    protected int codCiudad;
    protected int codPais;
    protected String descripcion;

    /**
     * Gets the value of the codCiudad property.
     * 
     */
    public int getCodCiudad() {
        return codCiudad;
    }

    /**
     * Sets the value of the codCiudad property.
     * 
     */
    public void setCodCiudad(int value) {
        this.codCiudad = value;
    }

    /**
     * Gets the value of the codPais property.
     * 
     */
    public int getCodPais() {
        return codPais;
    }

    /**
     * Sets the value of the codPais property.
     * 
     */
    public void setCodPais(int value) {
        this.codPais = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

}
