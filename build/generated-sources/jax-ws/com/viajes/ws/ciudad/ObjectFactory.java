
package com.viajes.ws.ciudad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.viajes.ws.ciudad package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchCiudadResponse_QNAME = new QName("http://ws.viajes/", "searchCiudadResponse");
    private final static QName _SearchCiudad_QNAME = new QName("http://ws.viajes/", "searchCiudad");
    private final static QName _ListCiudades_QNAME = new QName("http://ws.viajes/", "listCiudades");
    private final static QName _ActivoResponse_QNAME = new QName("http://ws.viajes/", "activoResponse");
    private final static QName _ListCiudadesResponse_QNAME = new QName("http://ws.viajes/", "listCiudadesResponse");
    private final static QName _Activo_QNAME = new QName("http://ws.viajes/", "activo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.viajes.ws.ciudad
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SearchCiudadResponse }
     * 
     */
    public SearchCiudadResponse createSearchCiudadResponse() {
        return new SearchCiudadResponse();
    }

    /**
     * Create an instance of {@link SearchCiudad }
     * 
     */
    public SearchCiudad createSearchCiudad() {
        return new SearchCiudad();
    }

    /**
     * Create an instance of {@link ListCiudades }
     * 
     */
    public ListCiudades createListCiudades() {
        return new ListCiudades();
    }

    /**
     * Create an instance of {@link ActivoResponse }
     * 
     */
    public ActivoResponse createActivoResponse() {
        return new ActivoResponse();
    }

    /**
     * Create an instance of {@link ListCiudadesResponse }
     * 
     */
    public ListCiudadesResponse createListCiudadesResponse() {
        return new ListCiudadesResponse();
    }

    /**
     * Create an instance of {@link Activo }
     * 
     */
    public Activo createActivo() {
        return new Activo();
    }

    /**
     * Create an instance of {@link Ciudad }
     * 
     */
    public Ciudad createCiudad() {
        return new Ciudad();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCiudadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchCiudadResponse")
    public JAXBElement<SearchCiudadResponse> createSearchCiudadResponse(SearchCiudadResponse value) {
        return new JAXBElement<SearchCiudadResponse>(_SearchCiudadResponse_QNAME, SearchCiudadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchCiudad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchCiudad")
    public JAXBElement<SearchCiudad> createSearchCiudad(SearchCiudad value) {
        return new JAXBElement<SearchCiudad>(_SearchCiudad_QNAME, SearchCiudad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListCiudades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listCiudades")
    public JAXBElement<ListCiudades> createListCiudades(ListCiudades value) {
        return new JAXBElement<ListCiudades>(_ListCiudades_QNAME, ListCiudades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activoResponse")
    public JAXBElement<ActivoResponse> createActivoResponse(ActivoResponse value) {
        return new JAXBElement<ActivoResponse>(_ActivoResponse_QNAME, ActivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListCiudadesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listCiudadesResponse")
    public JAXBElement<ListCiudadesResponse> createListCiudadesResponse(ListCiudadesResponse value) {
        return new JAXBElement<ListCiudadesResponse>(_ListCiudadesResponse_QNAME, ListCiudadesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activo")
    public JAXBElement<Activo> createActivo(Activo value) {
        return new JAXBElement<Activo>(_Activo_QNAME, Activo.class, null, value);
    }

}
