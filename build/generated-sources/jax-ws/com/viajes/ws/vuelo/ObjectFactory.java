
package com.viajes.ws.vuelo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.viajes.ws.vuelo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListVuelos_QNAME = new QName("http://ws.viajes/", "listVuelos");
    private final static QName _SearchVueloResponse_QNAME = new QName("http://ws.viajes/", "searchVueloResponse");
    private final static QName _ActivoResponse_QNAME = new QName("http://ws.viajes/", "activoResponse");
    private final static QName _SearchVuelo_QNAME = new QName("http://ws.viajes/", "searchVuelo");
    private final static QName _ListVuelosResponse_QNAME = new QName("http://ws.viajes/", "listVuelosResponse");
    private final static QName _Activo_QNAME = new QName("http://ws.viajes/", "activo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.viajes.ws.vuelo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListVuelos }
     * 
     */
    public ListVuelos createListVuelos() {
        return new ListVuelos();
    }

    /**
     * Create an instance of {@link SearchVueloResponse }
     * 
     */
    public SearchVueloResponse createSearchVueloResponse() {
        return new SearchVueloResponse();
    }

    /**
     * Create an instance of {@link ActivoResponse }
     * 
     */
    public ActivoResponse createActivoResponse() {
        return new ActivoResponse();
    }

    /**
     * Create an instance of {@link SearchVuelo }
     * 
     */
    public SearchVuelo createSearchVuelo() {
        return new SearchVuelo();
    }

    /**
     * Create an instance of {@link ListVuelosResponse }
     * 
     */
    public ListVuelosResponse createListVuelosResponse() {
        return new ListVuelosResponse();
    }

    /**
     * Create an instance of {@link Activo }
     * 
     */
    public Activo createActivo() {
        return new Activo();
    }

    /**
     * Create an instance of {@link Vuelo }
     * 
     */
    public Vuelo createVuelo() {
        return new Vuelo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListVuelos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listVuelos")
    public JAXBElement<ListVuelos> createListVuelos(ListVuelos value) {
        return new JAXBElement<ListVuelos>(_ListVuelos_QNAME, ListVuelos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchVueloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchVueloResponse")
    public JAXBElement<SearchVueloResponse> createSearchVueloResponse(SearchVueloResponse value) {
        return new JAXBElement<SearchVueloResponse>(_SearchVueloResponse_QNAME, SearchVueloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activoResponse")
    public JAXBElement<ActivoResponse> createActivoResponse(ActivoResponse value) {
        return new JAXBElement<ActivoResponse>(_ActivoResponse_QNAME, ActivoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchVuelo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "searchVuelo")
    public JAXBElement<SearchVuelo> createSearchVuelo(SearchVuelo value) {
        return new JAXBElement<SearchVuelo>(_SearchVuelo_QNAME, SearchVuelo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListVuelosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "listVuelosResponse")
    public JAXBElement<ListVuelosResponse> createListVuelosResponse(ListVuelosResponse value) {
        return new JAXBElement<ListVuelosResponse>(_ListVuelosResponse_QNAME, ListVuelosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.viajes/", name = "activo")
    public JAXBElement<Activo> createActivo(Activo value) {
        return new JAXBElement<Activo>(_Activo_QNAME, Activo.class, null, value);
    }

}
